# Wownero Python

A Python module for Wownero. This was copied from the [monero-python](https://github.com/monero-ecosystem/monero-python) library made for Monero.


Copyright (c) 2017-2022 Michał Sałaban <michal@salaban.info> and Contributors:
`lalanza808`_, `cryptochangements34`_, `atward`_, `rooterkyberian`_, `brucexiu`_,
`lialsoftlab`_, `moneroexamples`_, `massanchik`_, `MrClottom`_.

Copyright (c) 2016 The MoneroPy Developers (``monero/base58.py`` taken from `MoneroPy`_)

Copyright (c) 2011-2013 `pyca/ed25519`_ Developers (``monero/ed25519.py``)

Copyright (c) 2011 thomasv@gitorious (``monero/seed.py`` based on `Electrum`_)
